<?php


namespace App\Enums;


/**
 * Class EducationsType
 * @package App\enums
 */
class EducationsType
{
    public const SECONDARY_EDUCATION = 'SECONDARY_EDUCATION';

    public const SPECIAL_EDUCATION = 'SPECIAL_EDUCATION';

    public const HIGHER_EDUCATION = 'HIGHER_EDUCATION';
}