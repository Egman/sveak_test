<?php

namespace App\Command;

use App\Repository\ClientRepository;
use App\Service\CalculateScoreService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CalculateScoreCommand
 * @package App\Command
 */
class CalculateScoreCommand extends Command
{
    /**
     * @var CalculateScoreService
     */
    private $calculateScoreService;
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CalculateScoreCommand constructor.
     * @param CalculateScoreService $calculateScoreService
     * @param ClientRepository $clientRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(CalculateScoreService $calculateScoreService, ClientRepository $clientRepository, EntityManagerInterface $em)
    {
        $this->calculateScoreService = $calculateScoreService;
        $this->clientRepository = $clientRepository;
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:calculate-score')
            ->setDescription('Calculate score')
            ->addArgument('clientId', InputArgument::OPTIONAL, 'client id for calculate');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $clientId = $input->getArgument('clientId');

        $rows = $this->calculateScoreService->updateScores($clientId);

        if ($rows == null) {
            $output->write('Client id ' . $clientId . ' not found');
            return 0;
        }

        $table = new Table($output);
        $table
            ->setHeaders(['Id', 'Пользователь', 'Телефон', 'Почта', 'Образование', 'Согласен', 'Итог', 'Обновлено в БД'])
            ->setRows($rows);
        $table->render();

        return 0;
    }
}
