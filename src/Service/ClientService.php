<?php


namespace App\Service;


use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ClientService
 * @package App\Service
 */
class ClientService
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CalculateScoreService
     */
    private $calculateScoreService;

    /**
     * ClientService constructor.
     * @param ClientRepository $clientRepository
     * @param EntityManagerInterface $em
     * @param CalculateScoreService $calculateScoreService
     */
    public function __construct(ClientRepository $clientRepository, EntityManagerInterface $em, CalculateScoreService $calculateScoreService)
    {
        $this->clientRepository = $clientRepository;
        $this->em = $em;
        $this->calculateScoreService = $calculateScoreService;
    }


    /**
     * @param Client $client
     * @param bool $isCreate
     */
    public function editClient(Client $client, bool $isCreate = true): void
    {
        if ($isCreate) {
            $this->calculateScoreService->calculateScore($client);
        }

        $this->em->persist($client);
        $this->em->flush();
    }
}