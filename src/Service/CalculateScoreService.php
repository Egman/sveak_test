<?php


namespace App\Service;


use App\Entity\Client;
use App\Enums\EducationsType;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CalculateScoreService
 * @package App\Service
 */
class CalculateScoreService
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ClientService constructor.
     * @param ClientRepository $clientRepository
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     */
    public function __construct(ClientRepository $clientRepository, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->clientRepository = $clientRepository;
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * @param int|null $clientId
     * @return array|null
     */
    public function updateScores(int $clientId = null): ?array
    {
        $clients = [];
        $rows = [];
        $needFlush = false;

        if ($clientId) {
            $client = $this->clientRepository->find($clientId);
            if (!$client) {
                return null;
            }
            $clients[] = $client;

        } else {
            $clients = $this->clientRepository->findAll();
        }

        foreach ($clients as $client) {
            $oldScore = $client->getScore();

            $scores = $this->calculateScore($client);
            $name = $client->getName() . " " . $client->getSurname();
            $phoneScore = $client->getPhone() . " (" . $scores['phoneScore'] . ")";
            $emailScore = $client->getEmail() . " (" . $scores['emailScore'] . ")";
            $educationScore = $this->translator->trans($client->getEducation()) . " (" . $scores['educationScore'] . ")";
            $confirmRuleScore = $client->getConfirmedRuleText() . " (" . $scores['confirmRuleScore'] . ")";
            $isUpdateDB = 'Нет';

            if ($oldScore !== $client->getScore()) {
                $needFlush = true;
                $isUpdateDB = 'Да';
                $this->em->persist($client);
            }

            $rows[] = [$client->getId(), $name, $phoneScore, $emailScore, $educationScore, $confirmRuleScore, $scores['result'], $isUpdateDB];
        }

        if ($needFlush) {
            $this->em->flush();
        }

        return $rows;
    }

    /**
     * @param Client $client
     * @return array
     */
    public function calculateScore(Client $client): array
    {
        $phoneScore = $this->calculatePhoneScore($client->getPhone());
        $emailScore = $this->calculateEmailScore($client->getEmail());
        $educationScore = $this->calculateEducationScore($client->getEducation());
        $confirmRuleScore = $this->calculateConfirmRuleScore($client->getConfirmedRule());
        $result = $phoneScore + $emailScore + $educationScore + $confirmRuleScore;
        $client->setScore($result);

        return [
            'phoneScore' => $phoneScore,
            'emailScore' => $emailScore,
            'educationScore' => $educationScore,
            'confirmRuleScore' => $confirmRuleScore,
            'result' => $result,
        ];
    }

    /**
     * @param $phone
     * @return int
     */
    private function calculatePhoneScore($phone): int
    {
        $re = '/\+7\((\d{3})\)(.+)/m';
        preg_match_all($re, $phone, $matches, PREG_SET_ORDER, 0);
        if (!count($matches)) {
            return 1;
        }

        $code = intval($matches[0][1]);
        //$number = intval(str_replace('-','',$matches[0][2]));

        if (($code >= 920 && $code <= 929) || ($code >= 930 && $code <= 939)) {
            //megafon 902, 904, 908, 920..929, 930..934, 936..939, 950, 951, 999
            return 10;
        } elseif (($code >= 902 && $code <= 906) || ($code >= 960 && $code <= 969)) {
            //beeline 900, 902..906, 908, 909, 950, 951, 953, 960..969, 980, 983, 986
            return 5;
        } elseif (($code >= 910 && $code <= 919) || ($code >= 980 && $code <= 989)) {
            //mts 901, 902, 904, 908, 910..919, 950, 978, 980..989
            return 3;
        }

        return 1;
    }

    /**
     * @param $email
     * @return int
     */
    private function calculateEmailScore($email): int
    {
        $re = '/@(.+)\./m';
        preg_match_all($re, $email, $matches, PREG_SET_ORDER, 0);

        if (!count($matches)) {
            return 3;
        }
        $match = $matches[0][1];

        switch ($match) {
            case 'gmail':
                return 10;
            case 'yandex':
                return 8;
            case 'mail':
                return 6;
            default:
                return 3;
        }
    }

    /**
     * @param $education
     * @return int
     */
    private function calculateEducationScore($education): int
    {
        if ($education === EducationsType::HIGHER_EDUCATION) {
            return 15;
        } elseif ($education === EducationsType::SPECIAL_EDUCATION) {
            return 10;
        }

        return 5;
    }

    /**
     * @param $confirmRule
     * @return int
     */
    private function calculateConfirmRuleScore($confirmRule): int
    {
        return $confirmRule ? 4 : 0;
    }
}