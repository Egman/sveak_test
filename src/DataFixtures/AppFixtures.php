<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Enums\EducationsType;
use App\Service\CalculateScoreService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{

    /**
     * @var CalculateScoreService
     */
    private $calculateScoreService;

    /**
     * AppFixtures constructor.
     * @param CalculateScoreService $calculateScoreService
     */
    public function __construct(CalculateScoreService $calculateScoreService)
    {
        $this->calculateScoreService = $calculateScoreService;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $names = ['Аскольд', 'Роман', 'Филимон', 'Измаил', 'Савелий'];
        $surnames = ['Ермишин', 'Комолов', 'Гайдуков', 'Соловьев', 'Рыбаков'];
        $phones = ['+7(961)192-44-33', '+7(597)449-33-82', '+7(931)242-44-26', '+7(409)424-30-67', '+7(981)266-13-64', '+7(060)979-48-83', '+7(167)133-60-35', '+7(611)572-17-92', '+7(908)389-60-03', '+7(279)343-06-48'];
        $emails = ['frolov_ocv@mail.ru', 'reg9053341489@yandex.ru', 'dimonnn777@bk.ru', 'Vohahristenko@gmail.com', 'lapotko.den@bk.ru', 'shiro78@protonmail.com'];
        $educations = [EducationsType::HIGHER_EDUCATION, EducationsType::SPECIAL_EDUCATION, EducationsType::SECONDARY_EDUCATION];
        $confirm = [true, false];

        for ($i = 1; $i <= 50; $i++) {
            $client = new Client();
            $client->setName($names[array_rand($names)]);
            $client->setSurname($surnames[array_rand($surnames)]);
            $client->setPhone($phones[array_rand($phones)]);
            $client->setEmail($emails[array_rand($emails)]);
            $client->setEducation($educations[array_rand($educations)]);
            $client->setConfirmedRule($confirm[array_rand($confirm)]);
            $this->calculateScoreService->calculateScore($client);
            $manager->persist($client);
        }


        $manager->flush();
    }
}
