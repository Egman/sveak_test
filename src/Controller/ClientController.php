<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Service\ClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ClientController
 * @package App\Controller
 */
class ClientController extends AbstractController
{

    /**
     * @Route("/client/{client}", name="client")
     * @param Client|null $client
     * @return Response
     */
    public function client(Client $client = null): Response
    {
        return $this->render('client/showClient.html.twig', [
            'client' => $client
        ]);
    }

    /**
     * @Route("/edit/{client}", name="editClient")
     * @param Request $request
     * @param ClientService $clientService
     * @param Client|null $client
     * @return Response
     */
    public function edit(Request $request, ClientService $clientService, Client $client = null): Response
    {
        $isCreate = !$client;
        if ($isCreate) {
            $client = new Client();
        }

        $form = $this->createForm(ClientType::class, $client);
        $form->add($isCreate ? 'create' : 'save', SubmitType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $clientService->editClient($form->getData(), $isCreate);

            return $this->redirectToRoute('client', [
                'client' => $client->getId()
            ]);
        }

        return $this->render('client/index.html.twig', [
            'isCreate' => $isCreate,
            'client' => $client,
            'form' => $form->createView()
        ]);
    }
}
