<?php

namespace App\Controller;

use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @return Response
     */
    public function index(Request $request, ClientRepository $clientRepository): Response
    {
        $page = max(0, $request->query->getInt('page', 0) - 1);
        $paginator = $clientRepository->getClientPaginator($page);
        $pageCount = ceil($paginator->count() / ClientRepository::PAGINATOR_PER_PAGE);

        return $this->render('main/index.html.twig', [
            'clients' => $paginator,
            'currentPage' => $page + 1,
            'pageCount' => $pageCount
        ]);
    }
}
