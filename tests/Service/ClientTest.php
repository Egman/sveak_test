<?php

namespace App\Tests\Service;

use App\Entity\Client;
use App\Enums\EducationsType;
use App\Service\CalculateScoreService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ClientTest
 * @package App\Tests\Service
 */
class ClientTest extends WebTestCase
{
    /**
     * @dataProvider calculateScoreProvider
     * @param $phone
     * @param $email
     * @param $education
     * @param $confirmRule
     * @param $expected
     */
    public function testCalculateScore($phone, $email, $education, $confirmRule, $expected)
    {
        self::bootKernel();
        $container = self::$container;
        $calculateScoreService = $container->get(CalculateScoreService::class);

        $client = new Client();
        $client->setName('name')->setSurname('surname')->setPhone($phone)->setEmail($email)->setEducation($education)->setConfirmedRule($confirmRule);
        $calculateScoreService->calculateScore($client);
        $this->assertEquals($expected, $client->getScore());
    }

    public function calculateScoreProvider()
    {
        return [
            ['+7(931)999-99-99', 'test@gmail.com', EducationsType::HIGHER_EDUCATION, true, 10+10+15+4],
            ['+7(961)999-99-99', 'test@yandex.com', EducationsType::SPECIAL_EDUCATION, false, 5+8+10+0],
            ['+7(981)999-99-99', 'test@mail.com', EducationsType::SECONDARY_EDUCATION, true, 3+6+5+4],
            ['+7(971)999-99-99', 'test@temmail.com', EducationsType::HIGHER_EDUCATION, false, 1+3+15+0],
        ];
    }
}
